package com.arche.book.service;

import com.arche.book.model.Book;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service // business layer
public class BookService {

  private Map<Long, Book> books = new HashMap<>();
  private long nextId = 1;

  public BookService(){
    addBook(new Book("The Great Gatsby", "scott", 1925)); ///1
    addBook(new Book("1984", "orwell", 1949)); //2
    addBook(new Book("To Kill a MockingBird", "Lee", 1960)); //3
  }

  //add book -> start the application, these books will be added
  private void addBook(Book book){
    book.setId(nextId); //4
    books.put(nextId, book);
    System.out.println("the Book is added :: " +book);
    nextId++; //nextId = nextId + 1
  }

  //create
  public Book createBook(Book book){
    book.setId(nextId); //setting the Id for book
    books.put(nextId, book);
    nextId++;
    return book;
  }


  //read
  public Book getBookById(Long id){
    return books.get(id); //books.get(1)
  }

  //read
  public List<Book> getAllBooks(){
    return new ArrayList<>(books.values());
  }

  //update
  public Book updateBook(Long id, Book updatedBook){
    if(books.containsKey(id)){
      updatedBook.setId(id);
      books.put(id, updatedBook);
      return updatedBook;
    }
    return null;
  }

  //delete
  public void deleteBookById(Long id){
    books.remove(id);
  }

}
