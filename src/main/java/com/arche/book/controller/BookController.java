package com.arche.book.controller;


import com.arche.book.model.Book;
import com.arche.book.service.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


//localhost:8080/api/books/1
@RestController
@RequestMapping("/api/books")
public class BookController {

  //constructor based injection
  private final BookService bookService;

  public BookController(BookService bookService){
    this.bookService = bookService;
  }

  //create -> post
  @PostMapping
  public Book createBook(@RequestBody Book book){
    return bookService.createBook(book);
  }

  //read -> get
  @GetMapping
  public List<Book> getAllBooks(){
    return bookService.getAllBooks();
  }

  //read -> getById
  @GetMapping("/{id}")
  public Book getBookById(@PathVariable Long id){
    return bookService.getBookById(id);
  }

  //update -> put
  @PutMapping("/{id}")
  public Book updateBook(@PathVariable Long id, @RequestBody Book book){
    return bookService.updateBook(id, book);
  }

  //delete -> delete
  @DeleteMapping("/{id}")
  public void deleteBookById(@PathVariable Long id){
    bookService.deleteBookById(id);
  }
}
